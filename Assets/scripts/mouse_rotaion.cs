﻿using UnityEngine;
using System.Collections;

public class mouse_rotaion : MonoBehaviour {

	private Rigidbody body;
	private Camera view_camera;
	private float rotation_speed = 5f;
	private float y_rotation;
	private bool button_pressed;
	private Vector3 rotation;
	private UnityEngine.Quaternion rotation_calc;

	void Start () {
		body = GetComponent<Rigidbody>();
		view_camera = GetComponentInChildren<Camera>();
	}

	void Update(){

		y_rotation = Input.GetAxisRaw("Mouse X");
		rotation = new Vector3(0f, y_rotation, 0f) * rotation_speed;

		button_pressed = Input.GetMouseButton(1);
		if (button_pressed) {
			Cursor.visible = false;
			rotation_calc = body.rotation * Quaternion.Euler(rotation);
			body.MoveRotation(rotation_calc);
		}
	}
}
