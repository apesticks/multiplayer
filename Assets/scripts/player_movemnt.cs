﻿using UnityEngine;
using System.Collections;

public class player_movemnt : MonoBehaviour {

	public float move_speed = 15;
	private Vector3 direction;
	private Rigidbody body;

	void Start () {
		body = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		float x = Input.GetAxisRaw("Horizontal");
		float z = Input.GetAxisRaw("Vertical");

		Vector3 h = transform.right * x;
		Vector3 v = transform.forward * z;

		Vector3 velocity = (h + v).normalized * move_speed;
		body.MovePosition(body.position + velocity * Time.fixedDeltaTime);
	}
}
