﻿using UnityEngine;
using UnityEngine.Networking;

public class player_setup : NetworkBehaviour {

	[SerializeField]
	Behaviour[] components_to_disable;

	Camera scene_camera;
	
	void Start () {
		
		if(!isLocalPlayer){
			for(int i=0; i < components_to_disable.Length; i++){
				components_to_disable[i].enabled = false;
			}
		}
		else{
			scene_camera = Camera.main;
			if(scene_camera != null){
				scene_camera.gameObject.SetActive(false);
			}
		}
	}

	void OnDisable(){

		if(scene_camera != null){
			scene_camera.gameObject.SetActive(true);
		}
	}

}
